/**
 * Created by Suhas on 7/26/2016.
 */
var zmq = require('zmq');
/*sax*/
var dataHandlers='';
var configObj='';
var zmqPushPull = function (dataHandlerPath,config) {
        var queue = config.queue;
        configObj=config;
        dataHandlers  = require(dataHandlerPath);
        queue.forEach(function(queueItem){
                zmqMethods[queueItem.type](queueItem);
        })
};
var zmqMethods={
        pull:function(zmqPortObj) {
                var zmqSocket = zmq.socket(zmqPortObj.type);
                var recHost =configObj.recHost;
                if(zmqPortObj.recHost){
                        recHost=zmqPortObj.recHost
                }
                var zmqPortTcpIpUrl = 'tcp://'+recHost+':'+zmqPortObj.portNo;
                zmqSocket.bindSync(zmqPortTcpIpUrl, function (err) {
                        if (err)console.log(err.stack)
                });
                console.info(`adding a listener ${zmqPortTcpIpUrl} for the payload ${zmqPortObj.name}`);
                zmqSocket.on("message", function (message) {
                        try{
                                var jsonPayload = JSON.parse(message.toString());
                                processData(jsonPayload, zmqPortObj.name);
                        }catch(err){
                                console.error("Error while processing the message :")
                                if(message){
                                        console.log(message.toString())
                                }
                        }
                })
        },
        sub:function(zmqPortObj){
                var zmqSocket = zmq.socket(zmqPortObj.type);
                var recHost =configObj.recHost;
                if(zmqPortObj.recHost){
                        recHost=zmqPortObj.recHost
                }
                var zmqPortTcpIpUrl = 'tcp://'+recHost+':'+zmqPortObj.portNo;
                zmqSocket.bindSync(zmqPortTcpIpUrl, function (err) {
                        if (err)console.log(err.stack)
                });
                console.info(`adding a listener ${zmqPortTcpIpUrl} for the payload ${zmqPortObj.name}`);
                zmqSocket.on("message", function (message) {
                        try{
                                var jsonPayload = message.toString();
                                processData(jsonPayload, zmqPortObj.name);
                        }catch(err){
                                console.error("Error while processing the message :"+err.stack)
                                console.error(err.stack)
                                if(message){
                                        console.log(message.toString())
                                }
                        }
                })
        }
}
/*function startPulling(zmqPortObj) {
        var zmqSocket = zmq.socket(zmqPortObj.type);
        var recHost =configObj.recHost;
        if(zmqPortObj.recHost){
                recHost=zmqPortObj.recHost
        }
        var zmqPortTcpIpUrl = 'tcp://'+recHost+':'+zmqPortObj.portNo;
        zmqSocket.bindSync(zmqPortTcpIpUrl, function (err) {
                if (err)console.log(err.stack)
        });
        console.info(`adding a listener ${zmqPortTcpIpUrl} for the payload ${zmqPortObj.name}`);
        zmqSocket.on("message", function (message) {
                try{
                        var jsonPayload = JSON.parse(message.toString());
                        processData(jsonPayload, zmqPortObj.name);
                }catch(err){
                        console.error("Error while processing the message :")
                        if(message){
                                console.log(message.toString())
                        }
                }
        })
}*/

function processData(message, portName) {
        var handlerObjectName =portName+"Handler";
        var handler = dataHandlers[handlerObjectName];
        handler.saveData(message);
}
module.exports = {zmqPushPull: zmqPushPull};